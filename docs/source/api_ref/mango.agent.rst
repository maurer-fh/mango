mango.agent package
===================

Submodules
----------

mango.agent.core module
-----------------------

.. automodule:: mango.agent.core
   :members:
   :undoc-members:
   :show-inheritance:

mango.agent.role module
-----------------------

.. automodule:: mango.agent.role
   :members:
   :undoc-members:
   :show-inheritance:

Module contents
---------------

.. automodule:: mango.agent
   :members:
   :undoc-members:
   :show-inheritance:
